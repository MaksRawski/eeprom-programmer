#!/bin/bash

PORT=$@
[ -e $PORT ] || ( echo "No port provided!"; exit )

#Estabilish screen session
screen -L -Logfile screen.log -S arduino -d -m /dev/ttyACM* 115200 
screen -ls | grep 'arduino' > /dev/null && echo 'Session created successfully.' || echo 'Failed to open session on port ' $PORT "."

#for progress bar
NOBAR='....................'
BAR='####################'
dataSize=$(cat hexData | wc -w)
progress=0

sleep 2
screen -S arduino -X stuff "w"
sleep 0.1
screen -S arduino -X stuff "$(printf "%04x" $(cat hexData | wc -w))"

for i in $(cat hexData); do
    screen -S arduino -X stuff "$i"
    sleep 0.00015 #write byte takes ~110us
    (( progress++ ))
    j=$(( $progress*20 / $dataSize ))
    echo -ne "\r[${BAR:0:$j} ${NOBAR:$j:21}] $(( 100*$progress / $dataSize ))% ($progress/$dataSize)"
done

#screen -S arduino -X hardcopy ./screen.log.copy
sleep 0.1
# screen -S arduino -X quit
echo ""
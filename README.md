An Arduino EEPROM Programmer designed specifically for [W27C512](https://media.digikey.com/pdf/Data%20Sheets/Winbond%20PDFs/W27C512.pdf) but somewhat based on this project [https://github.com/StormTrooper/eeprom_programmer](https://github.com/StormTrooper/eeprom_programmer) as well as this one [https://github.com/beneater/eeprom-programmer](https://github.com/beneater/eeprom-programmer). 
It utilises Arduino Leonardo and 2x74HC595 (shift registers).
W27C512 requires 12V to enter programming mode so external 12V power supply is also required.


## Circuit
![circuit](https://gitlab.com/i4mz3r0/eeprom-programmer/-/raw/master/circuit.png)
For switching 12V you can use any circuit that you'd like as long as it's active low or you are upto making small changes in code. [Some examples of other switching circuits](https://electronics.stackexchange.com/questions/70214/switching-12v-with-an-active-low-5v-signal). As a side note i shall note that i have no idea how or why mine works and whether it should even do something. Though that's what worked for me.
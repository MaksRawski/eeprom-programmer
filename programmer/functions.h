#define SHIFT_DATA A0 //SER
#define SHIFT_LATCH A1 //RCLK
#define SHIFT_CLK A2 //SRCLK
#define CE A3
#define OE A4 

#define VPP 11 //OE pin=VPP 12v
#define VH 12  //Erase pin (A9) 12V 

#define DATASTARTPIN 2
#define DATAENDPIN 9

unsigned int err=0; //error count
unsigned int dataLength=0;

void setAddress(unsigned int address) {
	shiftOut(SHIFT_DATA, SHIFT_CLK, MSBFIRST, address >> 8);
	shiftOut(SHIFT_DATA, SHIFT_CLK, MSBFIRST, address);

	digitalWrite(SHIFT_LATCH, LOW);
	digitalWrite(SHIFT_LATCH, HIGH);
	digitalWrite(SHIFT_LATCH, LOW);
}

byte readByte(unsigned int address) {
	setAddress(address);
	digitalWrite(VH,HIGH);
	digitalWrite(CE,LOW);
	digitalWrite(VPP,HIGH);
	digitalWrite(OE,LOW);

	delayMicroseconds(1);

	//data bus is input
	for (int i=2; i<10; i++) {
		pinMode(i, INPUT);
	}

	byte data = 0;
	for (int i=9; i>=2; i--) {
		data = (data << 1) + digitalRead(i);
	}
	return data;
}

void writeByte(unsigned int address, byte data) {
	digitalWrite(VPP,LOW); //make sure that it's in programming mode
	digitalWrite(VH,HIGH); //A9 won't be 12V
	digitalWrite(CE,HIGH);
	digitalWrite(OE,HIGH); 

	setAddress(address);
	delayMicroseconds(5);

	//setup data
	for (int n=0; n<8; n++) {
		pinMode(n+2, OUTPUT);
		digitalWrite(n+2,( (data) & (1<<n) )>>n); //write nth bit of data to the n pin
	}
	delayMicroseconds(5);

	digitalWrite(CE, LOW);
	delayMicroseconds(100);
	digitalWrite(CE, HIGH);
	delayMicroseconds(5); //hold data and address for 5us

	//verify - implementation of smart programming alorithm 2 contained in W27C512's datasheet 
    byte a=readByte(address);
	if(a<16) Serial.print("0"); //print data with leading zeros
    Serial.print(a,HEX);

    if(a!=data){
		Serial.println("Failed to write! Trying again.");
		err++;
		if(err==25){
			Serial.println("FATAL ERROR! EEPROM ACTS UNRELIABLY!");
			Serial.println("Serial will automatically close in 5s.");
			delay(5000);
			Serial.end();
			exit(0);
		}
		writeByte(address,data);
	}
    else err=0;
}

void eraseEEPROM() {
	digitalWrite(CE,HIGH);
	digitalWrite(OE,HIGH);
	digitalWrite(VH,LOW);
	digitalWrite(VPP,LOW);

	setAddress(0);
	
	//data all ones
	for (int i=2; i<10; i++) {
		pinMode(i, OUTPUT);
		digitalWrite(i,HIGH);
	}
	delayMicroseconds(5);

	digitalWrite(CE, LOW);
	delay(100);
	digitalWrite(CE, HIGH);

	digitalWrite(VH,HIGH);
	digitalWrite(VPP,HIGH);

	Serial.println("");
	Serial.println("EEPROM successfully erased!");
}
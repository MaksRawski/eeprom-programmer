#include "functions.h"

/*
If command asks for input it will end with "(n):".
Where n is the number of characters that the command is expecting. Use leading zeros if necessary.
*/

//read upto 4 hex characters (16 bits) from serial and return actual value
unsigned int readFromSerial(unsigned int length){ 
	unsigned int i=0;
    unsigned char buffer[4];
	while(i<length){	
		if(Serial.available()>0) {
			buffer[i]=Serial.read();
			i++;
		}
	}
    return strtoul(buffer,nullptr,16);
}

void setup(){
    Serial.begin(115200);

	pinMode(SHIFT_DATA, OUTPUT);
	pinMode(SHIFT_CLK, OUTPUT);
	pinMode(SHIFT_LATCH, OUTPUT);

	digitalWrite(VH, HIGH); //VH will be 0v
	pinMode(VH, OUTPUT);

	digitalWrite(VPP, HIGH); //VPP will be 0v
	pinMode(VPP, OUTPUT);
	
	digitalWrite(CE,LOW);
	pinMode(CE,OUTPUT);
	
	digitalWrite(OE,LOW);
	pinMode(OE,OUTPUT);

}

void loop(){
    if(Serial.available()>0){
        char rx=Serial.read();
        switch (rx){

        case 'e':
        case 'E':
            {
                Serial.print("Erasing EEPROM in 3 seconds. Hit c to cancel.");
                unsigned int i=0;
                while(i<3){
                    delay(1000);
                    Serial.print(".");
                    if(Serial.available()>0){
                        char c=Serial.read();
                        if(c=='c'){
                            i=3;
                            Serial.println("Canceled erasing operation.");
                        } 
                    }
                    i++;
                }
                if(i==3) eraseEEPROM();
            }
            break;

        case 'r':
        case 'R':
            {
                Serial.print("Read 255 Bytes from: ");
                unsigned int offset=readFromSerial(4);
                Serial.println(offset,HEX);
                
                for(unsigned int i=0;i<=255;i++){
                    byte read=readByte(offset+i);
                    if(read<16) Serial.print("0"); //print data with leading zeros
                    Serial.print(read, HEX);
                    Serial.print(" ");
                    if((i+1)%16==0) Serial.println("");
                }
            }
            break;

        case 'w':
        case 'W':
            {
                Serial.print("Length of data (4): ");
                unsigned int length=readFromSerial(4);
                Serial.println(length,HEX);

                Serial.println("Data (2):");
                for(unsigned int i=0; i<length; i++){
                    writeByte(i,byte(readFromSerial(2))); //outputs written byte
                    Serial.print(" ");
                    if((i+1)%16==0) Serial.println("");
                }

                Serial.println("");
                Serial.println("Done!");
            }
            break;
        
        default:
            Serial.println("Select: Read, Write, Erase [R/W/E]");
            break;
        }

    }
}
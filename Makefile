DIR := programmer 
.PHONY: $(DIR)
all: $(DIR)
./%:
	arduino-cli compile --fqbn arduino:avr:leonardo $(DIR)
	arduino-cli upload -b arduino:avr:leonardo -p /dev/ttyACM* $(DIR)